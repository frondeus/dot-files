To bootstrap your config on new machine:

	alias config '/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
	git clone --bare git@gitlab.com:frondeus/dot-files.git $HOME/.cfg
